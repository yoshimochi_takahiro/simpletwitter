<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>簡易Twitter</title>
	</head>
	<body>
		<div class="header">
			<!-- ログインしていない場合はこっち -->
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>

			<!-- ログイン済みの場合はこっち -->
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>

		<div class="main-contents">
			<c:if test="${ not empty loginUser }">
				<!-- プロフィール -->
				<div class="profile">
					<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
					<div class="account">@<c:out value="${loginUser.account}" /></div>
					<div class="description"><c:out value="${loginUser.description}" /></div>
				</div>
			</c:if>

			<!-- ログイン処理に関するエラーメッセージ -->
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>

				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<!-- message絞込機能 -->
			<div class="date-area">
				<form action="./" method="get">
					日付：<input type="date" name="start" value="${start}" <c:out value="${start}" />>～
						  <input type="date" name="end" value="${end}" <c:out value="${end}" />>
						  <input type="submit" value="絞込">
				</form>
			</div>

			<!-- message投稿エリア -->
			<div class="form-area">
				<c:if test="${ isShowMessageForm }">
					<form action="message" method="post">
						いま、どうしてる？<br />
						<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
						<br />
						<input type="submit" value="つぶやく">（140文字まで）
					</form>
				</c:if>
			</div>

			<!-- message表示エリア -->
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="account-name">
						<span class="account">
							<a href="./?user_id=<c:out value="${message.userId}"/> ">
								<c:out value="${message.account}" />
							</a>
						</span>

						<span class="name">
							<c:out value="${message.name}" />
						</span>
					</div>

					<div class="text">
						<pre><c:out value="${message.text}" /></pre>
					</div>

					<div class="date">
						<fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>

					<div class="edit">
						<c:if test="${ loginUser.id == message.userId }">
							<form action="edit" method="get"><br />
								<a href="edit">
									<input type="hidden" name="message_id" value="${message.id}">
									<input type="submit" value="編集">
								</a>
							</form>
						</c:if>
					</div>

					<div class="delete">
						<c:if test="${ loginUser.id == message.userId }">
							<form action="deleteMessage" method="post"><br />
								<input type="hidden" name="id" value="${message.id}">
								<input type="submit" value="削除">
							</form>
						</c:if>
					</div>
				</div>

				<!-- comment投稿エリア -->
				<div class="form-area">
					<c:if test="${ not empty loginUser }">
						<form action="comment" method="post">
							返信<br />
							<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
							<br />
							<input type="hidden" name="message_id" id="id" value="${message.id}">
							<input type="submit" value="返信">（140文字まで）
						</form>
					</c:if>
				</div>

				<!-- comment表示エリア -->
				<div class="comments">
					<c:forEach items="${comments}" var="comment">
						<c:if test="${ comment.messageId == message.id }">
							<div class="comment">
								<div class="account-name">
									<span class="account"><c:out value="${comment.account}" /></span>
									<span class="name"><c:out value="${comment.name}" /></span>
								</div>
								<div class="text">
									<pre><c:out value="${comment.text}" /></pre>
								</div>
								<div class="date">
									<fmt:formatDate value="${comment.createdDate}"
										pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
							</div>
						</c:if>

					</c:forEach>
				</div>
			</c:forEach>
		</div>

		<div class="copyright">Copyright(c)Takahiro Yoshimochi</div>
		</div>
	</body>
</html>