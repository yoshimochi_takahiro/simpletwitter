package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {



	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		Message message = null;

		// 編集対象のつぶやきのmessage.idを取得
		String editMessageId = request.getParameter("message_id");

		// message.idに正しい値が入っているか判定し、正しければ対象のつぶやきを取得
		if(!StringUtils.isEmpty(editMessageId) && (editMessageId.matches("^[0-9]+$"))) {
			int id = Integer.parseInt(editMessageId);
			message = new MessageService().select(id);
		}

		// messageの中身が不正でないか判定
		if(message == null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();
		Message message = getMessage(request);

		if (isValid(message, errorMessages)) {
			try {
				new MessageService().update(message);

			} catch (NoRowsUpdatedRuntimeException e) {
				errorMessages.add("他の人によって更新されています。最新のデータを表示しました データを確認してください。");

			}
		}

		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}
		response.sendRedirect("./");
	}

	private Message getMessage(HttpServletRequest request) throws IOException,
	ServletException {

		Message message = new Message();
		message.setId(Integer.parseInt(request.getParameter("message_id")));
		message.setText(request.getParameter("text"));
		return message;
	}


	private boolean isValid(Message message, List<String> errorMessages) {
		String text = message.getText();

		if(StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");

		} else if(140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");

		} if(errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

}
