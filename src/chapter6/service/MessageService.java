package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	// 入力したmessageを受け取るための処理
	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);

		} catch(RuntimeException e) {
			rollback(connection);
			throw e;

		} catch(Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	// message一覧を取得するための処理
	public List<UserMessage> select(String userId, String start, String end) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			Integer id = null;
			if(!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}

			SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");

			if(!StringUtils.isBlank(start)) {
				start += " 00:00:00";

			} else {
				start = "2020/01/01 00:00:00";

			}

			if(!StringUtils.isBlank(end)) {
				end += " 23:59:59";

			} else {
				Calendar endNowDate = Calendar.getInstance();
				end = sdFormat.format(endNowDate.getTime());
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, id, start, end,  LIMIT_NUM);
			commit(connection);

			return messages;

		} catch(RuntimeException e) {
			rollback(connection);
			throw e;

		} catch(Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	// つぶやきの編集時に使用
	public Message select(int messageId) {
		Connection connection = null;

		try {
			connection = getConnection();
			Message message = new MessageDao().select(connection, messageId);
			commit(connection);

			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	public void update(Message message) {

		Connection connection = null;

		try {
			connection = getConnection();
			new MessageDao().update(connection, message);
			commit(connection);

		} catch(RuntimeException e) {
			rollback(connection);
			throw e;

		} catch(Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	public void delete(String messageId) {
		Connection connection = null;

		try {
			connection = getConnection();

			Integer id = null;
			if(!StringUtils.isEmpty(messageId)) {
				id = Integer.parseInt(messageId);
			}

			new MessageDao().delete(connection, id);
			commit(connection);

		} catch(RuntimeException e) {
			rollback(connection);
			throw e;

		} catch(Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}
}
