package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.UserComment;
import chapter6.dao.CommentDao;
import chapter6.dao.UserCommentDao;

public class CommentService {

	// 入力したmessageを受け取るための処理
	public void insert(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();
			new CommentDao().insert(connection, comment);
			commit(connection);

		} catch(RuntimeException e) {
			rollback(connection);
			throw e;

		} catch(Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	public List<UserComment> select(String start, String end) {

		Connection connection = null;
		try {
			connection = getConnection();

			SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");

			if(!StringUtils.isBlank(start)) {
				start += " 00:00:00";

			} else {
				start = "2020/01/01 00:00:00";

			}

			if(!StringUtils.isBlank(end)) {
				end += " 23:59:59";

			} else {
				Calendar endNowDate = Calendar.getInstance();
				end = sdFormat.format(endNowDate.getTime());
			}

			List<UserComment> comments = new UserCommentDao().select(connection, start, end);
			commit(connection);

			return comments;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;

		} catch(Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}
}
