package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.dao.UserDao;
import chapter6.utils.CipherUtil;

public class UserService {
	// signupに対する処理
	public void insert(User user) {

		Connection connection = null;
		try {
			// パスワード暗号化
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			connection = getConnection();

			new UserDao().insert(connection, user);

			commit(connection);

		} catch(RuntimeException e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	// loginに対する処理、
	public User select(String accountOrEmail, String password) {
		Connection connection = null;

		try {
			// パスワード暗号化
			String encPswwword = CipherUtil.encrypt(password);

			connection = getConnection();
			User user = new UserDao().select(connection, accountOrEmail, encPswwword);
			commit(connection);

			return user;

		} catch(RuntimeException e) {
			rollback(connection);
			throw e;

		} catch(Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	// user情報編集
	public User select(int userId) {
		Connection connection = null;

		try {
			connection = getConnection();
			User user = new UserDao().select(connection, userId);
			commit(connection);

			return user;

		} catch(RuntimeException e) {
			rollback(connection);
			throw e;

		} catch(Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	// user情報更新
	public void update(User user) {

		Connection connection = null;

		try {
			if(!StringUtils.isBlank(user.getPassword())) {
				// パスワード暗号化
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}
			connection = getConnection();
			new UserDao().update(connection, user);
			commit(connection);

		} catch(RuntimeException e) {
			rollback(connection);
			throw e;

		} catch(Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	// ユーザーが重複して登録されないためのバリデーションチェックに使用
	public User select(String account) {
		Connection connection = null;

		try {
			connection = getConnection();
			User user = new UserDao().select(connection, account);
			commit(connection);

			return user;

		} catch (RuntimeException e) {
	        rollback(connection);
	        throw e;

	    } catch (Error e) {
	        rollback(connection);
	        throw e;

	    } finally {
	        close(connection);
	    }
	}
}
